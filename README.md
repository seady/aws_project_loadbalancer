L'architecture comporte : 

 * une classe Server.java qui est un loadbalancer et qui crée un Worker dans sa méthode aWorker toufe fois que la taille SQS croît en multiple de 35 et se charge de réduire le nombre des Workers lorsque la taille SQS est diminuée de 35.
   la gestion des workers se fait automatiquement.
   Au démarrage,le serveur loadbalancer  : crée ou vide s'il existe une queue de message SQS "semigambi-QRequest",initialise le compteur de requête "k" à 0,incrémente le compteur de requête "k",met dans la queue "QRequest" un message "value_k" avec la valeur de l'entier suivit de la valeur de "k",crée une queue "QResponse-k.
  ---- Server.java écoute sur le port 8089----

 * une classe Worker.java qui va en boucle et retourne le résultat contenu dans AWS-SQS.

###################################################################

Le code source contient 2 projets Maven : AWS_Project_LoadBalancer pour le serveur loadbalancer et AWS_Project_Worker pour le Worker.
Chaque projet contient un launcher pour lancer les classes.
Lancez d'abord le launcher_Server puis launcher_Worker.

ces projets et codes sources peuvent être telecharger sur bitbucket à l'adresse : https://bitbucket.org/seady/aws_project_loadbalancer.git
       
###################################################################
Nous pouvons remarquer la rapidité du déploiement de son implémentation et l'élasticité du serveur mais la lenteur réside lors de la création d'un Worker et sa réponse à chaque fois que celui ci  envoit une requête,ce qui rend l'architecture moins performant.

En terme de scalabilité,l'application permet le passage à l'échelle car à chaque fois que le nombre de requête augmente,un Worker se crée automatiquement et inversement. 

En terme de tolérance au panne,je peux dire que l'application n'est pas vraiment tolérente au panne car la défaillance de la queue AWS-SQS peut entraîner la chute/clash de toute l'application.Mais nous pouvons faire confiance à AWS.   







#######bitbucket.org#######
**** Repo owner : Andy-Saleh Semigambi  ****
**** ID : seady ****
**** contact : semigand@yahoo.fr ****



All right reserved.